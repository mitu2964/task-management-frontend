import React from 'react';
import { useAuth } from '../hook/auth';

const Header = ({props}) => {
  const { logout, user } = useAuth({ middleware: 'auth' });
  const callLogout = async () =>{
    logout();
  }

  return (
    <>
      <div className='flex justify-between px-4 pt-4'>
        <h2>{props.name}</h2>
        <div className="flex items-center md:order-2">
          <button type="button" className="flex mr-3 text-sm bg-gray-800 rounded-full md:mr-0 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600" id="user-menu-button" aria-expanded="false" data-dropdown-toggle="user-dropdown" data-dropdown-placement="bottom">
            <span className="sr-only">Open user menu</span>
            <img className="w-8 h-8 rounded-full" src="/user.png" alt="user photo" />
          </button>
          <div className="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700 dark:divide-gray-600" id="user-dropdown">
            <div className="px-4 py-3">
              <span className="block text-sm text-gray-900 dark:text-white">{user?.name}</span>
              <span className="block text-sm text-gray-500 truncate dark:text-gray-400">{user?.email}</span>
            </div>
            <ul className="py-2" aria-labelledby="user-menu-button">
              <li>
                <button type='button' onClick={callLogout} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white">Sign out</button>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <script src="https://unpkg.com/flowbite@1.5.1/dist/flowbite.js"></script>
    </>
  )
}

export default Header;