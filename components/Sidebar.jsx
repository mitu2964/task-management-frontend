import Link from 'next/link';
import React from 'react';
import { FiSettings } from 'react-icons/fi';
import { RxLayers, RxPerson, RxSketchLogo } from 'react-icons/rx';

const Sidebar = ({ children }) => {

  return (
      <div className='flex'>
        <div className='fixed w-20 h-screen p-4 bg-white border-r-[1px] flex flex-col justify-between'>
          <div className='flex flex-col items-center'>
            <Link href='/'>
              <div className='inline-block p-3 text-white bg-purple-800 rounded-lg'>
                <RxSketchLogo size={20} />
              </div>
            </Link>
            <span className='border-b-[1px] border-gray-200 w-full p-2'></span>
            
            <Link href='/users'>
              <div className='inline-block p-3 my-4 bg-gray-100 rounded-lg cursor-pointer hover:bg-gray-200'>
                <RxPerson size={20} />
              </div>
            </Link>
            <Link href='/tasks'>
              <div className='inline-block p-3 my-4 bg-gray-100 rounded-lg cursor-pointer hover:bg-gray-200'>
                <RxLayers size={20} />
              </div>
            </Link>
            <Link href='/'>
              <div className='inline-block p-3 my-4 bg-gray-100 rounded-lg cursor-pointer hover:bg-gray-200'>
                <FiSettings size={20} />
              </div>
            </Link>
          </div>
        </div>
        <main className='w-full ml-20'>{children}</main>
      </div>
  );
};

export default Sidebar;
