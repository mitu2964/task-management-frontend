import React from 'react';
import { FaShoppingBag } from 'react-icons/fa';

const RecentOrders = () => {
  const data = [
    {
      id: 1,
      name: {
        first: 'John',
        last: 'Smith',
      },
      total: 2795.95,
      status: 'On Hold',
      method: 'PayPal',
      date: '15 Minutes ago',
    },
    {
      id: 2,
      name: {
        first: 'Chris',
        last: 'Adams',
      },
      total: 1195.95,
      status: 'Processing',
      method: 'PayPal',
      date: '23 Minutes ago',
    },
    {
      id: 3,
      name: {
        first: 'Sarah',
        last: 'Smith',
      },
      total: 495.85,
      status: 'Completed',
      method: 'Visa',
      date: '1 Hour ago',
    },
    {
      id: 4,
      name: {
        first: 'Joseph',
        last: 'Choo',
      },
      total: 150.45,
      status: 'Processing',
      method: 'MasterCard',
      date: '1 Hour ago',
    }]
    
  return (
    <div className='w-full col-span-1 relative lg:h-[70vh] h-[50vh] m-auto p-4 border rounded-lg bg-white overflow-scroll'>
      <h1>Recent Orders</h1>
      <ul>
        {data.map((order, id) => (
          <li
            key={id}
            className='flex items-center p-2 my-3 rounded-lg cursor-pointer bg-gray-50 hover:bg-gray-100'
          >
            <div className='p-3 bg-purple-100 rounded-lg'>
              <FaShoppingBag className='text-purple-800' />
            </div>
            <div className='pl-4'>
              <p className='font-bold text-gray-800'>${order.total}</p>
              <p className='text-sm text-gray-400'>{order.name.first}</p>
            </div>
            <p className='absolute text-sm lg:flex md:hidden right-6'>{order.date}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default RecentOrders;
