export default function Textarea({ disabled = false, className = '', ...props }) {
    return (
        <textarea
            disabled={disabled}
            className={`${className} outline-none border rounded border-gray-200 h-10 px-2`}
            {...props}
        ></textarea>
    )
}