import { usePathname } from 'next/navigation';
import Sidebar from '../components/Sidebar';
import '../styles/globals.css';

export default function App({ Component, pageProps }) {
  const pathname = usePathname();
  
  return (
    (pathname === '/login' || pathname === '/register') ?
      <Component {...pageProps} />
    : <Sidebar>
        <Component {...pageProps} />
    </Sidebar>
  );
}
