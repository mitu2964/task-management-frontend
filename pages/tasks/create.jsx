import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import Button from '../../components/Button'
import Errors from '../../components/Errors'
import Header from '../../components/Header'
import Input from '../../components/Input'
import Label from '../../components/Label'
import Textarea from '../../components/Textarea'
import { useAuth } from '../../hook/auth'
import axios from '../../lib/axios'

export default function create() {
  const { user } = useAuth();
  const router = useRouter();

  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [deadline, setDeadline] = useState('')
  const [errors, setErrors] = useState([]);

  const submitForm = async (event) => {
      event.preventDefault();
      const data = {
        user_id:user.id,
        title: title,
        description: description,
        deadline: deadline
      };
      setErrors([]);
      try {
        await axios
          .post('/api/tasks', JSON.stringify(data))
          .then((res) => {
            // console.log(res);
            router.push('/tasks');
          })
          .catch(error => {
              console.log(error)
              if (error.response.status !== 422) throw error
              setErrors(error.response.data.errors)
          })
      } catch (error) {
        console.log(error);
      }
  }
  return (
    <>
      <div className='min-h-screen bg-gray-100'>
        <Header props={{ name: "Create a new Task" }} />
        <div className='p-4'>
          <div className='w-full p-4 m-auto overflow-y-auto bg-white rounded-lg bitem'>
          <Link href="/tasks" className="px-4 py-2 font-semibold text-blue-700 bg-transparent border border-blue-500 hover:bg-blue-500 hover:text-white hover:border-transparent" style={{display: "inline-block",
    marginBottom: "20px"}}>Tasks</Link>
            <Errors className="mb-5" errors={errors} />
            <form onSubmit={submitForm} autoComplete="off">
                <div>
                    <Label htmlFor="title">Title<span style={{color:"red"}}>*</span></Label>
                    <Input
                        id="title"
                        type="text"
                        value={title}
                        className="block w-full mt-1"
                        onChange={event => setTitle(event.target.value)}
                        required
                        autoFocus
                        autoComplete="off"
                    />
                </div>

                <div className="mt-4">
                    <Label htmlFor="description">Description<span style={{color:"red"}}>*</span></Label>
                    <Textarea
                        id="description"
                        type="text"
                        value={description}
                        className="block w-full mt-1"
                        onChange={event => setDescription(event.target.value)}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label htmlFor="deadline">Deadline<span style={{color:"red"}}>*</span></Label>
                    <Input
                        id="deadline"
                        type="date"
                        value={deadline}
                        className="block w-full mt-1"
                        onChange={event => setDeadline(event.target.value)}
                        required
                    />
                </div>
                <div className="flex items-center justify-end mt-4">
                    <Button type='submit' className="ml-3">Save</Button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </>
  )
}
