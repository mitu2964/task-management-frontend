import Link from 'next/link';
import useSWR from 'swr';
import Header from "../../components/Header";
import axios from '../../lib/axios';
import React, { useState } from 'react'
import { useAuth } from '../../hook/auth';
import Button from '../../components/Button'
import { useRouter } from 'next/router'

const tasks = () => {
  const router = useRouter();
  const { user } = useAuth({ middleware: 'auth' });
  const { data } = useSWR('/api/tasks', () =>
        axios
            .get('/api/tasks')
            .then(res => res.data.data)
            .catch(error => {
                if (error.response.status !== 409) throw error
            }),
    );
    console.log(data)
  const { data:users } = useSWR('/api/users', () =>
      axios
          .get('/api/users')
          .then(res => res.data.data)
          .catch(error => {
              if (error.response.status !== 409) throw error
          }),
  )
  const [auser, setAuser] = useState();
  const [errors, setErrors] = useState([]);
  const submitForm = async (event) => {
    event.preventDefault();
    const data = {
      assign_to: auser,
      task_id: event.target[0].value
    }
    setErrors([]);
    axios
        .post('/api/assign-to-users', data)
        .then((res) => {
          router.reload();
        })
        .catch(error => {
          console.log(error)
          
            if (error.response.status !== 422) throw error
            setErrors(error.response.data.errors)
        })
  }
  const saveandsubmit = async (event,id) => {
    if(confirm('Are you sure?'))  event.preventDefault();
    const data = {
      task_id: id
    }
    setErrors([]);
    axios
        .post('/api/compete-task', data)
        .then((res) => {
          router.reload();
        })
        .catch(error => {
          console.log(error)
            if (error.response.status !== 422) throw error
            setErrors(error.response.data.errors)
        })
  }
  return (
    <div className='min-h-screen bg-gray-100'>
      <Header props={{ name: "Tasks" }} />
      <div className='p-4'>
        <div className='w-full p-4 m-auto overflow-y-auto bg-white rounded-lg bitem'>
          <Link href="/tasks/create" className="px-4 py-2 font-semibold text-blue-700 bg-transparent border border-blue-500 hover:bg-blue-500 hover:text-white hover:border-transparent" style={{display: "inline-block",
    marginBottom: "20px"}}>Create Task</Link>
          <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
              <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                  <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                      <tr>
                          <th scope="col" className="px-6 py-3">
                              Title
                          </th>
                          <th scope="col" className="px-6 py-3">
                              Description
                          </th>
                          <th scope="col" className="px-6 py-3">
                              Created By
                          </th>
                          <th scope="col" className="px-6 py-3">
                              Assign To
                          </th>
                          <th scope="col" className="px-6 py-3">
                              Status
                          </th>
                          <th scope="col" className="px-6 py-3">
                              Action
                          </th>
                      </tr>
                  </thead>
                  <tbody>
                    {data && data.map((item, id)=>(
                      <tr key={id} className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                          <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                              {item.title}
                          </th>
                          <td className="px-6 py-4">
                              {item.description}
                          </td>
                          <td className="px-6 py-4">
                              {item.created_by?.name}
                          </td>
                          <td className="px-6 py-4">
                            {item.assign_to?.name}
                          </td>
                          <td className="px-6 py-4">
                            {item.status}
                          </td>
                          <td className="px-6 py-4">
                              {
                                (item.status == 'open')?
                                <>
                                <form onSubmit={submitForm} style={{display:"flex"}}> 
                                <input type="hidden" name="task_id" value={item.id} />
                                  <select id="assign_to" onChange={event => setAuser(event.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option value="0" selected>Select a user</option>
                                    {
                                      (users && users.map((user)=>(
                                        (item.user_id !== user.id)?
                                          <option key={user.id} value={user.id}>{user.name}</option>
                                        :''
                                      )))
                                    }
                                  </select>
                                  <button type='submit' className="px-4 py-2 font-semibold text-blue-700 bg-transparent border border-blue-500 hover:bg-blue-500 hover:text-white hover:border-transparent">
                                    Assign
                                  </button>
                                </form>
                                </>:(item.status == 'in_progress' && item.assigned_to == user.id)?<>
                                  <Button onClick={()=> saveandsubmit(event, item.id)} type='button' className="ml-3">Complete</Button>
                                </>:''
                              }
                          </td>
                      </tr>
                    ))}
                  </tbody>
              </table>
          </div>
          


        </div>
      </div>
    </div>
  );
};

export default tasks;
