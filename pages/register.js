import Link from 'next/link'
import { useState } from 'react'
import Button from '../components/Button'
import Errors from '../components/Errors'
import Input from '../components/Input'
import Label from '../components/Label'
import { useAuth } from '../hook/auth'

export default function Register() {
    const { register } = useAuth({
        middleware: 'guest',
        redirectIfAuthenticated: '/users',
    })

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [password_confirmation, setPasswordConfirmation] = useState('')
    const [errors, setErrors] = useState([])

    const submitForm = event => {
        event.preventDefault()

        register({ name, email, password, password_confirmation, setErrors })
    }


    return (
        <>
            <div className={"w-1/2 mx-auto bg-white p-5 rounded-lg"}>
                <Errors className="mb-5" errors={errors} />
                <form onSubmit={submitForm} autoComplete="off">
                    <div>
                        <Label htmlFor="name">Name</Label>
                        <Input
                            id="name"
                            type="text"
                            value={name}
                            className="block w-full mt-1"
                            onChange={event => setName(event.target.value)}
                            required
                            autoFocus
                            autoComplete="off"
                        />
                    </div>

                    <div className="mt-4">
                        <Label htmlFor="email">Email</Label>
                        <Input
                            id="email"
                            type="email"
                            value={email}
                            className="block w-full mt-1"
                            onChange={event => setEmail(event.target.value)}
                            required
                        />
                    </div>

                    <div className="mt-4">
                        <Label htmlFor="password">Password</Label>
                        <Input
                            id="password"
                            type="password"
                            value={password}
                            className="block w-full mt-1"
                            onChange={event => setPassword(event.target.value)}
                            required
                        />
                    </div>
                    <div className="mt-4">
                        <Label htmlFor="password">Confirm Password</Label>
                        <Input
                            id="password_confirmation"
                            type="password"
                            value={password_confirmation}
                            className="block w-full mt-1"
                            onChange={event => setPasswordConfirmation(event.target.value)}
                            required
                        />
                    </div>
                    <div className="flex items-center justify-end mt-4">
                        <Link href="/login" className="text-sm text-gray-600 underline hover:text-gray-900">
                            Already registered?
                        </Link>
                        <Button className="ml-3">Register</Button>
                    </div>
                </form>
            </div>
        </>
    )
}