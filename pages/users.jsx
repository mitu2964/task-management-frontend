import React from 'react';
import { BsPersonFill } from 'react-icons/bs';
import useSWR from 'swr';
import Header from '../components/Header';
import axios from '../lib/axios';

const users = () => {
  const { data } = useSWR('/api/users', () =>
        axios
            .get('/api/users')
            .then(res => 
              res.data.data
              // delete & Store the object into storage
              // localStorage.removeItem('data');
              // localStorage.setItem("data", JSON.stringify(res.data.data));
            )
            .catch(error => {
                if (error.response.status !== 409) throw error
            }),
    )

  return (
    <div className='min-h-screen bg-gray-100'>
      <Header props={{ name: "Users" }} />
      <div className='p-4'>
        <div className='w-full p-4 m-auto overflow-y-auto bg-white rounded-lg bitem'>
          <div className='grid items-center justify-between grid-cols-2 p-2 my-3 cursor-pointer md:grid-cols-4 sm:grid-cols-3'>
            <span>Name</span>
            <span className='text-right sm:text-left'>Email</span>
            <span className='hidden md:grid'>Date</span>
          </div>
          <ul>
            {data && data.map((item, id) => (
                <li key={id} className='grid items-center justify-between grid-cols-2 p-2 my-3 rounded-lg cursor-pointer bg-gray-50 hover:bg-gray-100 md:grid-cols-4 sm:grid-cols-3'>
                    <div className='flex items-center'>
                        <div className='p-3 bg-purple-100 rounded-lg'>
                            <BsPersonFill className='text-purple-800' />
                        </div>
                        <p className='pl-4'>{item.name}</p>
                    </div>
                    <p className='text-right text-gray-600 sm:text-left'>{item.email}</p>
                    <p className='text-right text-gray-600 sm:text-left'>{item.created_at}</p>
                </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default users;
